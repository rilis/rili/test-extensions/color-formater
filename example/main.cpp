#include <rili/MakeUnique.hpp>
#include <rili/Test.hpp>
#include <rili/test/extensions/ColorFormater.hpp>
#include <string>

class Foo {
 public:
    virtual void foo() = 0;
    virtual void bar() = 0;
    virtual void foo(int) = 0;
    virtual void bar(int) = 0;
    virtual ~Foo() = default;
};

class FooMock : public Foo {
 public:
    MOCK0(void, foo)
    MOCK0(void, bar)
    MOCK1(void, foo, int)
    MOCK1(void, bar, int)
};

TEST(Fixture1, SomePass) { EXPECT_EQ(1, 1); }
TEST(Fixture1, SomeFailure) { EXPECT_EQ(1, 2); }
TEST(Fixture1, DISABLED_Something) {}
TEST(Fixture1, SKIP_Something) {}
TEST(Fixture1, SomeMockFail) {
    FooMock mock;

    EXPECT_CALL(mock, bar, []() {});
    mock.foo();
}

TEST(Fixture2, SomePass) { EXPECT_EQ(1, 1); }
TEST(Fixture2, SomeFailure) { EXPECT_EQ(1, 2); }
TEST(Fixture2, DISABLED_Something) {}
TEST(Fixture2, SKIP_Something) {}

TEST(Fixture2, SomeMockFail) {
    FooMock mock;

    EXPECT_CALL(mock, bar, [](int) {});
    mock.foo(6);
}

int main(int /*argc*/, char** /*argv*/) {
    rili::test::EventHandler::getInstance().changeFormater(rili::make_unique<rili::test::extensions::ColorFormater>());

    // to have vt100 colors when run in gitlab CI even if not valid console type detected
    rili::service::Console::instance().outputMode(rili::service::Console::OutputMode::VT100_16Colors, true);

    auto filter = [](std::string const& /*fixture*/, std::string const& name) {
        if (name.substr(0, 9) == "DISABLED_") {
            return rili::test::runner::FilteringResult::Disable;
        } else if (name.substr(0, 5) == "SKIP_") {
            return rili::test::runner::FilteringResult::Skip;
        } else {
            return rili::test::runner::FilteringResult::Run;
        }
    };

    rili::test::runner::run(filter);
    return 0;
}
