# color-formater

This is extension for  [rili test framework](https://gitlab.com/rilis/rili/test) which add colored output test results support which looks close to that known from gtest.

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

* [![build status](https://gitlab.com/rilis/rili/test-extensions/color-formater/badges/master/build.svg)](https://gitlab.com/rilis/rili/test-extensions/color-formater/commits/master)

