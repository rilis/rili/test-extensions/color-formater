#include <rili/Color.hpp>
#include <rili/test/extensions/ColorFormater.hpp>
#include <string>

namespace rili {
namespace test {
namespace extensions {

ColorFormater::ColorFormater(rili::service::ConsoleBase& console)
    : Formater(),
      m_console(console),
      m_previousFixtureName(),
      m_currentTestName(),
      m_failed(false),
      m_numberOfSucceedTests(0),
      m_numberOfFailedTests(0),
      m_numberOfDisabledTests(0),
      m_numberOfSkippedTests(0),
      m_testStart(Clock::now()),
      m_regressionStart(Clock::now()) {}

ColorFormater::ColorFormater() : ColorFormater(rili::service::Console::instance()) {
    if (!m_console.outputMode(rili::service::ConsoleBase::OutputMode::VT100_16Colors)) {
        m_console.outputMode(rili::service::ConsoleBase::OutputMode::WINDOWS_API);
    }
}

void ColorFormater::testSkipped(const std::string& fixture, const std::string& scenario) noexcept {
    m_numberOfSkippedTests++;
    m_console << rili::service::console::fg(rili::color::ANSI16(2));
    if (m_previousFixtureName != fixture && !m_previousFixtureName.empty()) {
        m_console << "[----------]" << rili::stream::endl;
    }
    m_console << rili::service::console::fg(rili::color::ANSI16(6)) << "[   SKIP   ] " << rili::service::console::fg()
              << fixture << "." << scenario << rili::stream::endl;
    m_previousFixtureName = fixture;
}

void ColorFormater::testDisabled(const std::string& fixture, const std::string& scenario) noexcept {
    m_numberOfDisabledTests++;
    m_console << rili::service::console::fg(rili::color::ANSI16(2));
    if (m_previousFixtureName != fixture && !m_previousFixtureName.empty()) {
        m_console << "[----------]" << rili::stream::endl;
    }
    m_console << rili::service::console::fg(rili::color::ANSI16(3)) << "[ DISABLED ] " << rili::service::console::fg()
              << fixture << "." << scenario << rili::stream::endl;
    m_previousFixtureName = fixture;
}

void ColorFormater::createFailed(std::string const& reason) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "  Failure at 'construction': " << reason
              << rili::service::console::fg() << rili::stream::endl;
    m_failed = true;
}

void ColorFormater::beforeFailed(std::string const& reason) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "  Failure at 'before': " << reason
              << rili::service::console::fg() << rili::stream::endl;
    m_failed = true;
}

void ColorFormater::runFailed(std::string const& reason) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "  Failure at 'run': " << reason
              << rili::service::console::fg() << rili::stream::endl;
    m_failed = true;
}

void ColorFormater::afterFailed(std::string const& reason) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "  Failure at 'after': " << reason
              << rili::service::console::fg() << rili::stream::endl;
    m_failed = true;
}

void ColorFormater::startTest(const std::string& fixture, const std::string& scenario,
                              const std::string& type) noexcept {
    m_currentTestName = fixture + "." + scenario;
    if (type != "") {
        m_currentTestName += "<" + type + ">";
    }
    m_console << rili::service::console::fg(rili::color::ANSI16(2));
    if (m_previousFixtureName != fixture && !m_previousFixtureName.empty()) {
        m_console << "[----------]" << rili::stream::endl;
    }
    m_failed = false;
    m_testStart = Clock::now();
    m_previousFixtureName = fixture;
    m_console << "[ RUN      ] " << rili::service::console::fg() << m_currentTestName << rili::stream::endl;
}

void ColorFormater::endTest() noexcept {
    if (m_failed) {
        m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "[     FAIL ] "
                  << rili::service::console::fg() << m_currentTestName
                  << rili::service::console::fg(rili::color::ANSI16(6)) << " ["
                  << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - m_testStart).count() << "ms]"
                  << rili::service::console::fg() << rili::stream::endl;
        m_numberOfFailedTests++;
    } else {
        m_console << rili::service::console::fg(rili::color::ANSI16(2)) << "[       OK ] "
                  << rili::service::console::fg() << m_currentTestName
                  << rili::service::console::fg(rili::color::ANSI16(6)) << " ["
                  << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - m_testStart).count() << "ms]"
                  << rili::service::console::fg() << rili::stream::endl;
        m_numberOfSucceedTests++;
    }
    m_currentTestName = std::string();
    m_failed = false;
}

void ColorFormater::startTests() noexcept {
    m_failed = false;
    m_numberOfFailedTests = 0;
    m_numberOfSucceedTests = 0;
    m_numberOfDisabledTests = 0;
    m_numberOfSkippedTests = 0;
    m_regressionStart = Clock::now();
    m_console << rili::service::console::fg(rili::color::ANSI16(2)) << "[==========] Tests started"
              << rili::service::console::fg() << rili::stream::endl;
}

void ColorFormater::endTests() noexcept {
    auto totalTests = m_numberOfFailedTests + m_numberOfSucceedTests + m_numberOfDisabledTests + m_numberOfSkippedTests;
    m_console << rili::service::console::fg(rili::color::ANSI16(2)) << "[==========]" << rili::stream::endl;
    if (m_numberOfSucceedTests > 0) {
        m_console << "[  PASSED  ] " << m_numberOfSucceedTests << "/" << totalTests << rili::stream::endl;
    }

    if (m_numberOfSkippedTests > 0) {
        m_console << rili::service::console::fg(rili::color::ANSI16(6)) << "[ SKIPPED  ] " << m_numberOfSkippedTests
                  << "/" << totalTests << rili::stream::endl;
    }

    if (m_numberOfDisabledTests > 0) {
        m_console << rili::service::console::fg(rili::color::ANSI16(3)) << "[ DISABLED ] " << m_numberOfDisabledTests
                  << "/" << totalTests << rili::stream::endl;
    }

    if (m_numberOfFailedTests > 0) {
        m_console << rili::service::console::fg(rili::color::ANSI16(1)) << "[  FAILED  ] " << m_numberOfFailedTests
                  << "/" << totalTests << rili::stream::endl;
    }

    m_numberOfFailedTests = 0;
    m_numberOfSucceedTests = 0;
    m_numberOfDisabledTests = 0;
    m_numberOfSkippedTests = 0;
    m_previousFixtureName.clear();
    m_console << rili::service::console::fg(rili::color::ANSI16(2)) << "[==========] Tests finished in "
              << rili::service::console::fg(rili::color::ANSI16(6))
              << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - m_regressionStart).count() << "ms"
              << rili::service::console::fg() << rili::stream::endl;
}

void ColorFormater::expectFailed(const std::string& file, const std::string& line,
                                 const std::string& content) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(6))
              << "  Failure at: " << rili::service::console::fg(rili::color::ANSI16(3)) << file << "@" << line << ":"
              << rili::stream::endl
              << rili::service::console::fg(rili::color::ANSI16(1)) << "    " << content << rili::service::console::fg()
              << rili::stream::endl;
    m_failed = true;
}

void ColorFormater::unexpectedCall(std::string const& file, std::string const& line, void const* objectId,
                                   const std::string& signature, const std::string& arguments) noexcept {
    m_console << rili::service::console::fg(rili::color::ANSI16(6))
              << "  Unexpected call: " << rili::service::console::fg(rili::color::ANSI16(1)) << signature << "{"
              << objectId << "}" << rili::stream::endl
              << rili::service::console::fg(rili::color::ANSI16(6))
              << "    * on mock defined in: " << rili::service::console::fg(rili::color::ANSI16(3)) << file << "@"
              << line << rili::stream::endl;
    if (!arguments.empty()) {
        m_console << rili::service::console::fg(rili::color::ANSI16(6))
                  << "    * with args: " << rili::service::console::fg(rili::color::ANSI16(1)) << arguments
                  << rili::stream::endl;
    }
    m_console << rili::service::console::fg();
    m_failed = true;
}

}  // namespace extensions
}  // namespace test
}  // namespace rili
