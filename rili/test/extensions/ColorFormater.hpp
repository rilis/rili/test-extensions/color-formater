#pragma once
/** @file */

#include <chrono>
#include <cstdint>
#include <rili/service/Console.hpp>
#include <rili/test/Formater.hpp>
#include <string>

namespace rili {
namespace test {
namespace extensions {

/**
 * @brief Formater, which results are like in BasicFormater but with colors
 */
class ColorFormater final : public rili::test::Formater {
 public:
    /**
     * @brief Constructs ColorFormater, given in parameter console will be used to flush tests results
     * @param console
     */
    explicit ColorFormater(rili::service::ConsoleBase& console);

    /**
     * @brief constructs ColorFormater and use rili::service::Console to flush tests results
     *
     * @note this constructor modify rili::service::Console instance - try set mode which fits to your OS/Terminal
     * avaliable features. If you want manually set mode of console, use explicit ctor with parameter.
     */
    ColorFormater();
    virtual ~ColorFormater() = default;

 public:
    /// @cond INTERNAL
    void testSkipped(std::string const& fixture, std::string const& scenario) noexcept override;
    void testDisabled(std::string const& fixture, std::string const& scenario) noexcept override;
    void afterFailed(const std::string& reason) noexcept override;
    void beforeFailed(const std::string& reason) noexcept override;
    void createFailed(const std::string& reason) noexcept override;
    void endTest() noexcept override;
    void endTests() noexcept override;
    void runFailed(const std::string& reason) noexcept override;
    void startTest(std::string const& fixture, std::string const& scenario, std::string const& type) noexcept override;
    void startTests() noexcept override;

 public:
    void expectFailed(std::string const& file, std::string const& line, const std::string& content) noexcept override;
    void unexpectedCall(std::string const& file, std::string const& line, void const* objectId,
                        const std::string& signature, const std::string& arguments) noexcept override;
    /// @endcond INTERNAL

 private:
    rili::service::ConsoleBase& m_console;
    std::string m_previousFixtureName;
    std::string m_currentTestName;
    bool m_failed;

    uint_fast32_t m_numberOfSucceedTests;
    uint_fast32_t m_numberOfFailedTests;
    uint_fast32_t m_numberOfDisabledTests;
    uint_fast32_t m_numberOfSkippedTests;

    typedef std::chrono::high_resolution_clock Clock;
    Clock::time_point m_testStart;
    Clock::time_point m_regressionStart;
};
}  // namespace extensions
}  // namespace test
}  // namespace rili
